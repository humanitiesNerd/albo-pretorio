Lezione 0:

* Usiamo un editor, e.g. Visual Studio Code (code.visualstudio.com) che ha anche dei plugin, ma potete usare qualsiasi editor (anche nano);
* Ci serve naturalmente un browser (Firefox, Chrome, Safari ecc.);
* scarichiamo nodejs da nodejs.org (la versione LTS) o da riga di comando, per una distribuzione debian based ;

```
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
```
N.B: potremmo aver bisogno dei pacchetti gcc g++ make

```
sudo apt-get update
sudo apt-get install nodejs
```

- poi installiamo npm:

N.B: potremmo aver bisogno del pacchetto build-essential

```
sudo apt-get install npm
```

per visualizzare la versione e controllare che sia andato tutto ok, diamo: 

```
nodejs -v
```

e

```
npm -v
```

- creiamo una cartella Example 

```
mkdir Example
cd Example
npm init
```

- ora inseriamo il nome del pacchetto in "name: example"
poi dare sempre invio e rispondere yes alla domanda "is it ok?"

Vedremo il package.json dentro la nostra cartella Example.
Ora dentro Example possiamo creare il nostro primo file index.js
