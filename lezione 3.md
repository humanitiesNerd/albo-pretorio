Lezione4:

Soffermiamoci in questa lezione sull'uso di PostgreSQL, un completo DBMS ad oggetti rilasciato con licenza libera, che ci servirà in futuro per salvare i dati acquisiti con le tecniche di web scraping. 

Installiamo innanzitutto pgadmin3, un software di gestione del database, con interfaccia grafica:

```sudo apt install pgadmin3```

installiamo il modulo PostgreSQL per la dipendenza, da usare nel nostro script:

```npm install --save pg```

Creiamo un database di nome 'esempio':

```createdb esempio```

E' importante impostare, in PostgreSQL, una password per l'utente:

```
sudo -u user_name psql esempio
ALTER USER user_name WITH PASSWORD 'mypassword';
```

Creiamo un file savedata.js e scriviamoci il seguente codice:


```
const request = require('request-promise');
const cheerio = require('cheerio');
const pg = require('pg');

const URL='https://www.imdb.com/title/tt0102926/ ?ref_=nv_sr_1';
const pool = new pg.Pool({
    user: 'user_name',
    host: '127.0.0.1',
    database: 'esempio',
    password: 'mypassword',
    port: '5432'});

const createTables = () => {
    const queryText =
          `CREATE TABLE IF NOT EXISTS
                  films(
                    id integer PRIMARY KEY,
                    titolo VARCHAR(128) NOT NULL
                  )`;
            
    pool.query(queryText)
            
        .then((res) => {
            console.log(res);
        })
            
        .catch((err) => {
            console.log(err);
            pool.end();
        });           
}

const insertTable = (mydata) => {
    
    const queryText = INSERT INTO films (id, titolo) VALUES ($1, $2);
              
    const values = [
        1,
        mydata
    ];
    
    pool.query(queryText, values)
    
        .then((res) => {
            pool.end();
        })
              
        .catch((err) => {
            pool.end();
        });
}

createTables();

(async () => {

    const response = await request(URL);

    let $=cheerio.load(response);
    let title=$('div[class="title_wrapper"] > h1').text();  
    console.log(title);
    insertTable(title);
})()

```

ora eseguiamo con:

```
node savedata.js
```

Come possiamo facilmente notare, al contenuto del file index.js visto nell'ultima lezione abbiamo aggiunto la dipendenza del modulo 'pg' e le relative funzioni giusto per connettersi al nostro database 'esempio', creare una tabella di nome 'films' ed aggiungere il primo record che contiene il titolo del film ricavato dalla variabile 'title'.
