Proviamo a salvare i dati in un file .CSV, facendo uso della libreria json2csv.

I riferimenti alla libreria sono questi:
https://www.npmjs.com/package/json2csv 
https://github.com/zemirco/json2csv

Nella cartella del nostro progetto diamo:

`npm install json2csv --save`

facciamo una copia del file `index5fs.js` e rinominiamola in `index6csv.js`.

Nella [pagina](https://github.com/zemirco/json2csv) github, alla sezione 'json2csv parser (Synchronous API)', copiamo la stringa:

```
const Json2csvParser = require('json2csv').Parser;
```

e aggiungiamola nel file `index6csv.js` 

Sempre nella pagina github della libreria, diamo uno sguardo ad 'Example 1' e copiamo le due righe:

```
const json2csvParser = new Json2csvParser({ fields });
const csv = json2csvParser.parse(myCars);
```
andiamo ad inserirle nel file `index6csv.js`, modificandolo come segue:

```
const request = require('request-promise');
const cheerio = require('cheerio');
const fs = require('fs');
const Json2csvParser = require('json2csv').Parser;//per usare la libreria che scrive il file CSV

const URLS=[
'https://www.imdb.com/title/tt0102926/?ref_=nv_sr_1', 
'https://www.imdb.com/title/tt1477834/?ref_=nv_sr_1'
];

(async () => {
    let moviesData = [];
    for(let movie of URLS) {
        const response = await request(   
            {
                uri: movie,
                headers: { 
                    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0'  
                }
            });

        let $=cheerio.load(response);
        let title=$('div[class="title_wrapper"] > h1').text();
        let poster=$('div[class="poster"] > a > img').attr('src');
        
        let names = [];
        $('div[class="credit_summary_item"] a[href^="/name/"]').each((i, elm) => {
            let name = $(elm).text();
            names.push(name);
        });

        moviesData.push({
            title,
            poster,
            names
        });

        const fields=['title','poster'];// sono i campi che inseriremo nel file .csv
        const json2csvParser = new Json2csvParser({ fields });
        const csv = json2csvParser.parse(moviesData);

        //fs.writeFileSync('./data.json', JSON.stringify(moviesData), 'utf-8');
        fs.writeFileSync('./data.csv', csv, 'utf-8');

        console.log(moviesData);
    }
})()
```

proviamo a dare `node index6csv.js` dalla shell, e vedremo che si è aggiunto il file `data.csv` con i nostri campi indicati nell'array 'fields'.

Se vogliamo salvare su una tabella .csv tutti i dati trovati in 'moviesData', basta non indicare i campi 'fields'.
Proviamo dunque a modificare questa parte del sorgente:

```
const fields=['title','poster'];// sono i campi che inseriremo nel file .csv
const json2csvParser = new Json2csvParser({ fields });
const csv = json2csvParser.parse(moviesData);
```

in questa:

```
const json2csvParser = new Json2csvParser();
const csv = json2csvParser.parse(moviesData);
```
riproviamo a dare `node index6csv.js` dalla shell, e diamo una occhiata al nuovo file data.csv che ora presenta tutti i dati cercati.
