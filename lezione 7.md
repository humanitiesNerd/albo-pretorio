Ora vediamo come fare il download delle immagini.

Creiamo una copia del nostro file `index5fs.js` e rinominiamola `index7image.js`.

Nel file `index7image.js` aggiungiamo la libreria 'request' (riferimento: https://github.com/request/request):

```
const request = require('request');
```

e cambiamo la vecchia `request` in `requestPromise`.

ora modifichiamo l'array URLS riscrivendolo cosi':

```
const URLS=[
    { 
        url: 'https://www.imdb.com/title/tt0102926/?ref_=nv_sr_1',
        id: 'the_silence_of_the_lambs'
    }, 
    { 
        url: 'https://www.imdb.com/title/tt1477834/?ref_=nv_sr_1',
        id: 'gone_girl'
    }
];
```

e aggiungiamo (attenti al tipo di virgoletta/apostrofo tra le parentesi):

```
let file = fs.createWriteStream(${movie.id}.jpg);
```

Diamo uno sguardo alla libreria 'request' su https://github.com/request/request, nella sezione 'Streaming' abbiamo:

```
request('http://google.com/doodle.png').pipe(fs.createWriteStream('doodle.png'))
```

Dunque ora dobbiamo adattare il nostro codice:

```
        let stream = request({
            uri: poster,
            headers: {
                'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0' 
            }
        })
        .pipe(file);
```

Il nostro file `index7image.js` appare ora cosi':

```
const requestPromise = require('request-promise');
const cheerio = require('cheerio');
const fs = require('fs');
const request = require('request');

const URLS=[
    { 
        url: 'https://www.imdb.com/title/tt0102926/?ref_=nv_sr_1',
        id: 'the_silence_of_the_lambs'
    }, 
    { 
        url: 'https://www.imdb.com/title/tt1477834/?ref_=nv_sr_1',
        id: 'gone_girl'
    }
];

(async () => {
    let moviesData = [];
    for(let movie of URLS) {
        const response = await requestPromise(   
            {
                uri: movie.url,
                headers: { 
                    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0'  
                }
            });

        let $=cheerio.load(response);
        let title=$('div[class="title_wrapper"] > h1').text();
        let poster=$('div[class="poster"] > a > img').attr('src');
        
        let names = [];
        $('div[class="credit_summary_item"] a[href^="/name/"]').each((i, elm) => {
            let name = $(elm).text();
            names.push(name);
        });

        moviesData.push({
            title,
            poster,
            names
        });

        let file = fs.createWriteStream(${movie.id}.jpg);
        let stream = request({
            uri: poster,
            headers: {
                'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0' 
            }
        })
        .pipe(file);
        //fs.writeFileSync('./data.json', JSON.stringify(moviesData), 'utf-8');
        //console.log(moviesData);
    }
})()
```
diamo `node index7image.js` dalla shell, ed avremo nella nostra directory le due immagini .jpg
