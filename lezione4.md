In generale, i siti su cui fare web scraping con i metodi di richiesta http sono:

* quelli basati su API
* quelli che hanno una Basic Authentication
* dove vi sono files da scaricare

La 'browser automation' è una tecnica che consente di controllare il browser attraverso il codice invece che con l'interazione umana.

Si scrive dunque meno codice che nelle request method, ma è certamente piu' lento delle request method.

Una libreria per la browser automation, creata e mantenuta dal GoogleChrome team è puppeteer, di cui parleremo nelle prossime lezioni.

Guardando il nostro codice, possiamo notare:

* la libreria `request-promise`, che è una  libreria per metodi di richiesta HTTP semplificata con supporto alle Promise
* la libreria `cheerio` che è fondamentalmente jQuery orientato a node.js
	
Dare uno sguardo al [tutorial](https://github.com/request/request-promise) per comprendere il funzionamento della libreria request-promise.


Vai in Firefox e premi shift+ctrl+i per visualizzare gli strumenti di analisi.

Vai ora sulla tab 'network' che si trova nella parte bassa del browser affianco ad altre tab come 'memory', 'console', 'inspector', 'debugger' ecc.

Nella colonna 'Domain' prova a cliccare sulla prima chiamata 'www.imdb.com' e fai attenzione alla 'Request Headers' che si trova nella parte destra (sotto 'Response Headers').

Rivediamo ora il nostro primo `index.js` e riscriviamolo nominandolo `index5.js`, dove al posto della semplice `request(URL)` ora inseriremo i seguenti dati copiati dall'header della richiesta:

```
const request = require('request-promise');
const cheerio = require('cheerio');

const URL='https://www.imdb.com/title/tt0102926/?ref_=nv_sr_1';

(async () => {

    const response = await request({
        uri: URL,
        headers: {               
         'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0'
        }
    });

    let $=cheerio.load(response);
    let title=$('div[class="title_wrapper"] > h1').text();
  
    console.log(title);
})()
```

Come vediamo il risultato è lo stesso, ed abbiamo la stessa 'response' da passare poi a cheerio.

Ora colleghiamoci al seguente link:
https://www.w3schools.com/jquery/jquery_ref_selectors.asp

e diamo una occhiata ai jQuery Selectors.


Vai in Firefox e premi shift+ctrl+i per visualizzare gli strumenti di analisi.
All'altezza della tab 'network' precedentemente usata, vi è la tab 'inspector': clicchiamoci sopra e vedremo visualizzato il codice della pagina (in Google Chrome la tab è 'elements').

Ora andiamo sulla tab 'console' e scriviamo: `$('div[class="poster"] > a > img')`

Premiamo invio e vedremo visualizzare l'elemento richiesto, individuato nel DOM della pagina attraverso l'utilizzo di jQuery

Diamo ora una occhiata ai link: api.jquery.com e api.jquery.com/attr/

Scriviamo nella console: `$('div[class="poster"] > a > img').attr('src')`

e vedremo visualizzare l'attributo src dell'elemento.

Proviamo ad esercitarci cercando di individuare gli elementi della pagina attraverso la console, cosi' da poterli gestire successivamente da codice.
Infatti ora sarà facile ricavare i dati direttamente da codice: proviamo ad aggiungere, nel nostro file `index5.js`, le seguenti righe:

```
let poster=$('div[class="poster"] > a > img').attr('src');
console.log(title, poster);
```

Proviamo ora a scrivere nella console:

```
let names = [];
$('div[class="credit_summary_item"] a[href^="/name/"]').each((i, elm) => {
let name = $(elm).text();
names.push(name);
});
console.log(names)
```

premendo invio avremo l'array con i nomi degli attori.

Inseriamo quest'ultima parte di codice nel nostro index5.js, che ora diventa:

```
const request = require('request-promise');
const cheerio = require('cheerio');

const URL='https://www.imdb.com/title/tt0102926/?ref_=nv_sr_1';

(async () => {
    const response = await request(   
        {
        uri: URL,
        headers: {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0'  
        }
    });
```

In generale, i siti su cui fare web scraping con imetodi di richiesta http sono:

* quelli basati su API
* quelli che hanno una Basic Authentication
* dove vi sono files da scaricare

La 'browser automation' è una tecnica che consente di controllare il browser attraverso il codice invece che con l'interazione umana.
Si scrive dunque meno codice che nelle request method, ma è certamente piu' lento delle request method.

Una libreria per la browser automation, creata e mantenuta dal GoogleChrome team è puppeteer, di cui parleremo nelle prossime lezioni.

Guardando il nostro codice, possiamo notare
- la libreria request-promise, che è una  libreria per metodi di richiesta HTTP semplificata con supporto alle Promise
- la libreria cheerio che è fondamentalmente jQuery orientato a node.js
Dare uno sguardo al tutorial per comprendere il funzionamento della libreria request-promise: https://github.com/request/request-promise


Vai in Firefox e premi shift+ctrl+i per visualizzare gli strumenti di analisi.
Vai ora sulla tab 'network' che si trova nella parte bassa del browser affianco ad altre tab come 'memory', 'console', 'inspector', 'debugger' ecc.
Nella colonna 'Domain' prova a cliccare sulla prima chiamata 'www.imdb.com' e fai attenzione alla 'Request Headers' che si trova nella parte destra (sotto 'Response Headers').
Rivediamo ora il nostro primo index.js e riscriviamolo nominandolo index5.js, dove al posto della semplice 'request(URL)' ora inseriremo i seguenti dati copiati dall'header della richiesta:

```
const request = require('request-promise');
const cheerio = require('cheerio');

const URL='https://www.imdb.com/title/tt0102926/?ref_=nv_sr_1';

(async () => {

    const response = await request({
        uri: URL,
        headers: {               
         'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0'
        }
    });

    let $=cheerio.load(response);
    let title=$('div[class="title_wrapper"] > h1').text();
  
    console.log(title);
})()
```

Come vediamo il risultato è lo stesso, ed abbiamo la stessa 'response' da passare poi a cheerio.

Ora colleghiamoci al seguente link:
https://www.w3schools.com/jquery/jquery_ref_selectors.asp

e diamo una occhiata ai jQuery Selectors.


Vai in Firefox e premi shift+ctrl+i per visualizzare gli strumenti di analisi.
All'altezza della tab 'network' precedentemente usata, vi è la tab 'inspector': clicchiamoci sopra e vedremo visualizzato il codice della pagina (in Google Chrome la tab è 'elements').

Ora andiamo sulla tab 'console' e scriviamo:
$('div[class="poster"] > a > img')

Premiamo invio e vedremo visualizzare l'elemento richiesto, individuato nel DOM della pagina attraverso l'utilizzo di jQuery

Diamo ora una occhiata ai link: api.jquery.com e api.jquery.com/attr/

Scriviamo nella console:
$('div[class="poster"] > a > img').attr('src')

e vedremo visualizzare l'attributo src dell'elemento.

Proviamo ad esercitarci cercando di individuare gli elementi della pagina attraverso la console, cosi' da poterli gestire successivamente da codice.
Infatti ora sarà facile ricavare i dati direttamente da codice: proviamo ad aggiungere, nel nostro file index5.js, le seguenti righe:
```
let poster=$('div[class="poster"] > a > img').attr('src');
console.log(title, poster);
```
Proviamo ora a scrivere nella console:

```
let names = [];
$('div[class="credit_summary_item"] a[href^="/name/"]').each((i, elm) => {
let name = $(elm).text();
names.push(name);
});
console.log(names)
```

premendo invio avremo l'array con i nomi degli attori.

Inseriamo quest'ultima parte di codice nel nostro index5.js, che ora diventa:

```
const request = require('request-promise');
const cheerio = require('cheerio');

const URL='https://www.imdb.com/title/tt0102926/?ref_=nv_sr_1';

(async () => {
    const response = await request(   
        {
        uri: URL,
        headers: {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0'  
        }
    });

	let $=cheerio.load(response);
    let title=$('div[class="title_wrapper"] > h1').text();
    let poster=$('div[class="poster"] > a > img').attr('src');
        
    let names = [];
    $('div[class="credit_summary_item"] a[href^="/name/"]').each((i, elm) => {
        let name = $(elm).text();
        names.push(name);
    });

    console.log(title, poster, names);
})()
```
e, dopo aver dato `node index5.js` dalla shell, vedremo visualizzare il titolo, il link ed i nomi degli attori.

Ora ci creiamo una struttura dati per mantenere le nostre ricerche, magari aggiungendo un altro url, e successivamente le salviamo in modo permanente.

Riscriviamo il nostro file e nominiamolo ora `index5fs.js`:

```
const request = require('request-promise');
const cheerio = require('cheerio');

const URLS=[
'https://www.imdb.com/title/tt0102926/?ref_=nv_sr_1', 
'https://www.imdb.com/title/tt1477834/?ref_=nv_sr_1'
];

(async () => {
    let moviesData = [];
    for(let movie of URLS) {//per ogni film
        const response = await request(   
            {
                uri: movie,
                headers: { 
                    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0'  
                }
            });

        let $=cheerio.load(response);
        let title=$('div[class="title_wrapper"] > h1').text();
        let poster=$('div[class="poster"] > a > img').attr('src');
        
        let names = [];
        $('div[class="credit_summary_item"] a[href^="/name/"]').each((i, elm) => {
            let name = $(elm).text();
            names.push(name);
        });

        moviesData.push({
            title,
            poster,
            names
        });

        console.log(moviesData);
    }//per ogni film
})()

```
Ora salviamo i risultati in un file json, utilizzando la libreria `fs`.

importiamo la libreria con:

`const fs = require('fs');`

inseriamo, dopo la riga `console.log(moviesData)`, la seguente chiamata:

```
fs.writeFileSync('./data.json', JSON.stringify(moviesData), 'utf-8');
```

e dopo aver dato `node index5fs.js` dalla shell, sara' creato il file data.json con i nostri dati della ricerca.

Esiste un metodo veloce per individuare elementi direttamente dalla pagina html che osserviamo dal browser.

Se, infatti, in una pagina ispezioniamo un elemento, quando si apre lo strumento per sviluppatori/debug possiamo ulteriormente navigare nella tab inspector (su Firefox) o elements (su Chrome), dove cliccando con il tasto destro possiamo anche copiare il 'selector' dell'elemento prescelto.

Ad esempio su Chrome, nella tab 'elements' se clicchiamo con il tasto destro del mouse su un elemento, dal menu 'copy > copy selector' potremo ottenerne il selector
che viene copiato nella clipboard. 
Cosi' come in precedenza, dal codice, abbiamo individuato l'elemento:

```
let title=$('div[class="title_wrapper"] > h1').text();
```

potremo ora individuare un elemento a piacere usando il suo selector copiato tramite gli strumenti del browser, come sopra:
```
let mioelemento=$('QUI_INCOLLIAMO_IL_SELECTOR_MEMORIZZATO_DA_CHROME').text().trim();
```
